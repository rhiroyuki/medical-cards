require 'rails_helper'

feature 'user sees all cards at homepage' do
  scenario 'successfully' do
    card = Card.create(front: "Oi", back: "Hello")
    card2 = Card.create(front: "Gato", back: "Cat")
    visit root_path
    expect(page).to have_content card.front
    expect(page).to have_content card.back
    expect(page).to have_content card2.front
    expect(page).to have_content card.back
  end
end
