require 'rails_helper'
feature 'user creates a new card' do
  scenario 'sucessfully' do
    visit root_path
    click_on 'Create new card'
    fill_in 'Front view', with: 'Oi'
    fill_in 'Back view', with: 'Hello'
    click_on 'Create Card'
    expect(page).to have_content 'Oi'
    expect(page).to have_content 'Hello'
  end
end
