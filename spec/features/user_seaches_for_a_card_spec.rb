require 'rails_helper'

feature 'user searches for a card' do
  scenario 'successfully' do
    card = Card.create(front: 'Oi', back: 'Hello')
    visit root_path
    fill_in 'Search for', with: 'Oi'
    click_on 'Search card'
    expect(page).to have_content 'Oi'
    expect(page).to have_content 'Hello'
  end
end
