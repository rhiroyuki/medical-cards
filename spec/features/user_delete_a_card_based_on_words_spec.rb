require 'rails_helper'

feature 'user deletes a card' do
  scenario 'successfully' do
    card = Card.create(front: "Oi", back: "Hello")
    visit root_path
    fill_in 'Search for', with: 'Oi'
    click_on 'Delete card'
    expect(page).to have_no_content "Oi"
    expect(page).to have_no_content "Hello"
  end
end
