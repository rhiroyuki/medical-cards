class CardsController < ApplicationController
  def index
    @flag = params[:search].blank?
    @cards = Card.search params[:search]
  end

  def new
    @card = Card.new
  end

  def create
    @card = Card.new front: params[:card][:front], back: params[:card][:back]
    @card.save
    redirect_to @card
  end

  def show
    @card = Card.find params[:id]
  end

  def destroy
    @cards = Card.find params[:id]
    @cards.delete
    redirect_to cards_path
  end
end
