class Card < ApplicationRecord
  validates_presence_of [:front, :back]
  def self.search(termo)
    if termo
      all.select { |card| card.include?(termo) }
    else
      all
    end
  end

  def include?(text)
    front.upcase.include?(text.upcase) ||
      back.upcase.include?(text.upcase)
  end
end
